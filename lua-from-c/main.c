#include <stdio.h>
#include <lua.h>
#include <lauxlib.h>
#include <lualib.h>
#include <stdlib.h>

#define ADD_FILE "add.lua"
#define ADD_FUNCTION "add"

int main(int argc, char *argv[])
{
    int ret = -1;
    lua_State *L = NULL;

    L = luaL_newstate();
    if (L == NULL) {
      fprintf(stderr, "Unable to get Lua context\n");
      return -1;
    }

    luaL_openlibs(L);

    ret = luaL_loadfile(L, "add.lua");
    if (ret != LUA_OK) {
      fprintf(stderr, "Unable to open lua file %s\n", ADD_FILE);
      return -1;
    }

    ret = lua_pcall(L, 0, 0, 0);
    if (ret != LUA_OK) {
        return -1;
    }

    lua_getglobal(L, ADD_FUNCTION);
    if (lua_isnil(L, -1)) {
      fprintf(stderr, "Unable to find function %s\n", ADD_FUNCTION);
      return -1;
    }

    lua_pushinteger(L, atoi(argv[1]));
    lua_pushinteger(L, atoi(argv[2]));

    ret = lua_pcall(L, 2, 1, 0);
    if (ret != LUA_OK) {
        fprintf(stderr, "Unable to execute function %s [%d]\n", ADD_FUNCTION, ret);
      return -1;
    }

    if (lua_isinteger(L, -1)) {
        ret = lua_tointeger(L, -1);
        printf("%d\n", ret);
    }

    return 0;
}
