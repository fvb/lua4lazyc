file(GLOB SOURCES "*.c")
add_executable(add ${SOURCES})
set_property(TARGET add PROPERTY C_STANDARD 11)
target_link_libraries(add PRIVATE ${LUA_LIBRARIES})
target_include_directories(add PRIVATE ${LUA_INCLUDE_DIR})

configure_file("add.lua" "add.lua" COPYONLY)

