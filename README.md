# Lua for Lazy C developers

These are a couple of small examples that accompany the 2023 FOSDEM talk [Lua
for the Lazy C
developer](https://fosdem.org/2023/schedule/event/lua_for_the_lazy_c_developer/).

You'll need to install the Lua 5.3 development libraries on your system. Then
do:

```
mkdir build
cd build
cmake ..
make
```

All examples can be executed from the build directory.
If something doesn't work please open an issue and I'll try to give you a hand.
