#include <lua.h>
#include <lauxlib.h>
#include <stdio.h>

static int subtract(lua_State *L)
{
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    lua_pushinteger(L, a-b);
    return 1;
}

static const struct luaL_Reg c_arithmetic[] = {
    {"subtract", subtract},
    {NULL, NULL},
};

int main(void)
{
    int ret = -1;
    lua_State *L = luaL_newstate();

    luaL_newlib(L, c_arithmetic);
    lua_setglobal(L, "c_arithmetic");

    luaL_loadfile(L, "subtract.lua");
    if (lua_pcall(L, 0, 0, 0) != LUA_OK) {
        return -1;
    }

    lua_getglobal(L, "lua_subtract");
    if (lua_isnil(L, -1)) {
        fprintf(stderr, "Unable to find lua_subtract function\n");
        return -1;
    }

    lua_pushinteger(L, 5);
    lua_pushinteger(L, 3);

    ret = lua_pcall(L, 2, 1, 0);
    if (ret != LUA_OK) {
        fprintf(stderr, "Unable to execute lua_subtract function [%d]\n", ret);
        return -1;
    }

    if (lua_isinteger(L, -1)) {
        ret = lua_tointeger(L, -1);
        printf("%d\n", ret);
    }

    return 0;
}
