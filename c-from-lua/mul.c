#include <lua.h>
#include <lauxlib.h>

static int multiply(lua_State *L)
{
    int a = lua_tointeger(L, 1);
    int b = lua_tointeger(L, 2);
    lua_pushinteger(L, a*b);
    return 1;
}

static const struct luaL_Reg arithmetic [] = {
    {"multiply", multiply },
    {NULL, NULL}
};

int luaopen_arithmetic(lua_State *L)
{
    luaL_newlib(L, arithmetic);
    return 1;
}
