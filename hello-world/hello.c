#include <stdio.h>
#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

int main(void)
{
    lua_State *L = NULL;

    L = luaL_newstate();
    if (L == NULL) {
        fprintf(stderr, "Unable to get Lua context\n");
        return -1;
    }

    luaL_openlibs(L);

    lua_getglobal(L, "print");
    lua_pushstring(L, "Hello, World");
    if (lua_pcall(L, 1, 0, 0) != LUA_OK) {
        fprintf(stderr, "Unable to call \"print\" function\n");
        return -1;
    }

    return 0;
}
